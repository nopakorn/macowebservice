﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Maco.Models
{
    public class LogBindingModel
    {
        public int UserId { get; set; }
        public string ErrorCode { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime SerachedDateTime { get; set; }
        public string Country { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public int SerialNumber { get; set; }
       
    }
}