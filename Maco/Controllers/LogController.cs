﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Maco.Models;
using Maco.Providers;
using Maco.Results;
namespace Maco.Controllers
{
    [Authorize]
    [RoutePrefix("api/Log")]
    public class LogController : ApiController
    {
        // POST api/Log/CreateLog
        [AllowAnonymous]
        [Route("CreateLog")]
        public async Task<IHttpActionResult> CreateLog(Log model)
        {
            //string newRegistrationId = null;
            DateTime created = DateTime.Now;
           
            Log logModel = new Log()
            {
                UserId = model.UserId,
                ErrorCode = model.ErrorCode,
                Country = model.Country,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                Type = model.Type,
                Model = model.Model,
                SerialNumber = model.SerialNumber,
                CreatedDateTime = created,
                SearchedDateTime = created
            };

            string result = await InsertLogModel(logModel);

            return Ok();
        }

        private async Task<string> InsertLogModel(Log model)
        {
            using (var ctx = new MacoEntities())
            {
                try
                {
                    ctx.Logs.Add(model);
                    ctx.SaveChanges();
                }
                catch (Exception)
                {
                    return "Error save data";

                }

            }

            return "Finished";
        }
    }
}
